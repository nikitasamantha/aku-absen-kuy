package com.nikitasamantha_10191065.aku_absenkuy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class IzinActivity extends AppCompatActivity {

    Toolbar toolbarKembali;
    EditText etJenisIzin, etMulai, etSelesai, etKeterangan;
    Button btnAjukan;
    RecyclerView rvIzin;
    DatePickerDialog datePickerDialog;
    SimpleDateFormat simpleDateFormat;
    String tanggalMulai, tanggalSelesai;
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izin);

        sharedPreferences = getSharedPreferences("AKU",MODE_PRIVATE);
        simpleDateFormat = new SimpleDateFormat("yyy-MM-dd");
        etJenisIzin = findViewById(R.id.etJenisIzin);
        etMulai = findViewById(R.id.etMulai);
        etSelesai = findViewById(R.id.etSelesai);
        etKeterangan = findViewById(R.id.etKeterangan);
        btnAjukan = findViewById(R.id.btnAjukan);
        rvIzin = findViewById(R.id.rvIzin);
        toolbarKembali = findViewById(R.id.toolbarKembali);
        toolbarKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(IzinActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        etMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateMulai();
            }
        });
        etSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateSelesai();
            }
        });
        btnAjukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitAjukan();
            }
        });


    }
    private void showDateMulai() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                etMulai.setText(simpleDateFormat.format(newDate.getTime()));
                tanggalMulai = simpleDateFormat.format(newDate.getTime());
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    private void showDateSelesai() {
        Calendar calendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, month, dayOfMonth);
                etSelesai.setText(simpleDateFormat.format(newDate.getTime()));
                tanggalSelesai = simpleDateFormat.format(newDate.getTime());
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    private void submitAjukan(){
        JSONObject datajson = new JSONObject();
        try {
            datajson.put("user_id",sharedPreferences.getString("user_id",null));
            datajson.put("jenis",etJenisIzin.getText().toString());
            datajson.put("tanggal_mulai",tanggalMulai);
            datajson.put("tanggal_selesai",tanggalSelesai);
            datajson.put("keterangan",etKeterangan.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, getString(R.string.url)+"izin",datajson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Toast.makeText(IzinActivity.this,response.getString("keterangan"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        overridePendingTransition(0, 0);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(IzinActivity.this, "Gagal Terhubung", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(IzinActivity.this);
        rq.add(request);
    }

}