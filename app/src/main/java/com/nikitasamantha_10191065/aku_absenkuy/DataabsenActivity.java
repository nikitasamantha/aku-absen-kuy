package com.nikitasamantha_10191065.aku_absenkuy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DataabsenActivity extends AppCompatActivity {

    Toolbar toolbarKembali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dataabsen);

        toolbarKembali = findViewById(R.id.toolbarKembali);
        toolbarKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DataabsenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}