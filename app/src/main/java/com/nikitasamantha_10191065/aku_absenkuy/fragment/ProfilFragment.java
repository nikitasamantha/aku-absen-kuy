package com.nikitasamantha_10191065.aku_absenkuy.fragment;

import static android.content.Context.MODE_PRIVATE;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.nikitasamantha_10191065.aku_absenkuy.IzinActivity;
import com.nikitasamantha_10191065.aku_absenkuy.LoginActivity;
import com.nikitasamantha_10191065.aku_absenkuy.MainActivity;
import com.nikitasamantha_10191065.aku_absenkuy.R;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfilFragment extends Fragment {

    Button btnLogout, btnUbahAlamat, btnUbahNotelp;
    TextView tvNama, tvDepartemen, tvJabatan, tvNIP, tvEmail, tvAlamat, tvNotelp, tvAlamatEdit, tvNotelpEdit;
    CircleImageView imgProfil;
    Dialog dialog;
    EditText etAlamat, etNotelp;
    String alamat, notelp;

    public ProfilFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        get_data();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profil, container, false);
        imgProfil = view.findViewById(R.id.imgProfil);
        tvNama = view.findViewById(R.id.tvNama);
        tvDepartemen = view.findViewById(R.id.tvDepartemen);
        tvJabatan = view.findViewById(R.id.tvJabatan);
        tvNIP = view.findViewById(R.id.tvNIP);
        tvEmail = view.findViewById(R.id.tvEmail);
        tvAlamat = view.findViewById(R.id.tvAlamat);
        tvNotelp = view.findViewById(R.id.tvNotelp);
        tvAlamatEdit = view.findViewById(R.id.tvAlamatEdit);
        tvAlamatEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editAlamatDialog();
            }
        });
        tvNotelpEdit = view.findViewById(R.id.tvNotelpEdit);
        tvNotelpEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editNotelpDialog();
            }
        });
        dialog = new Dialog(getContext());
        btnLogout = view.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }
    private void get_data(){
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("AKU", MODE_PRIVATE);
        String url = getString(R.string.url)+"getdatauser/"+sharedPreferences.getString("user_id", null);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("Berhasill")){
                                Glide.with(getContext()).load(getString(R.string.gambar)+response.getJSONObject("data").getString("foto")).into(imgProfil);
                                tvNama.setText(response.getJSONObject("data").getString("name"));
                                tvDepartemen.setText(response.getJSONObject("data").getString("departemen"));
                                tvJabatan.setText(response.getJSONObject("data").getString("jabatan"));
                                tvNIP.setText(response.getJSONObject("data").getString("nip"));
                                tvEmail.setText(response.getJSONObject("data").getString("email"));
                                tvAlamat.setText(response.getJSONObject("data").getString("alamat"));
                                tvNotelp.setText(response.getJSONObject("data").getString("no_telp"));
                                alamat = response.getJSONObject("data").getString("alamat");
                                notelp = response.getJSONObject("data").getString("no_telp");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        RequestQueue rq = Volley.newRequestQueue(getContext());
        rq.add(request);

    }
    private void editAlamatDialog(){
        dialog.setContentView(R.layout.dialog_edit_alamat);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        etAlamat = dialog.findViewById(R.id.etAlamat);
        etAlamat.setText(alamat);
        btnUbahAlamat = dialog.findViewById(R.id.btnUbahAlamat);
        btnUbahAlamat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("AKU", MODE_PRIVATE);
                String editAlamat = etAlamat.getText().toString();
                String url = getResources().getString(R.string.url)+ "ubahalamat";
                String user_id = sharedPreferences.getString("user_id", null);
                JSONObject data = new JSONObject();
                try {
                    data.put("alamat", editAlamat);
                    data.put("user_id", user_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, data,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    tvAlamat.setText(response.getString("alamat"));
                                    dialog.dismiss();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                RequestQueue rq = Volley.newRequestQueue(getContext());
                rq.add(request);
            }
        });
        dialog.show();
    }
    private void editNotelpDialog(){
        dialog.setContentView(R.layout.dialog_edit_notelp);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        etNotelp = dialog.findViewById(R.id.etNotelp);
        etNotelp.setText(notelp);
        btnUbahNotelp = dialog.findViewById(R.id.btnUbahNotelp);
        btnUbahNotelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("AKU", MODE_PRIVATE);
                String editNotelp = etNotelp.getText().toString();
                String url = getResources().getString(R.string.url)+ "ubahnotelp";
                String user_id = sharedPreferences.getString("user_id", null);
                JSONObject data = new JSONObject();
                try {
                    data.put("no_telp", editNotelp);
                    data.put("user_id", user_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, data,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    tvNotelp.setText(response.getString("alamat"));
                                    dialog.dismiss();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
                RequestQueue rq = Volley.newRequestQueue(getContext());
                rq.add(request);
            }
        });
        dialog.show();
    }
}