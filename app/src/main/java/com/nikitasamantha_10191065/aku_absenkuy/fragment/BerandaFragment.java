package com.nikitasamantha_10191065.aku_absenkuy.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.nikitasamantha_10191065.aku_absenkuy.AktivitaspegawaiActivity;
import com.nikitasamantha_10191065.aku_absenkuy.DataabsenActivity;
import com.nikitasamantha_10191065.aku_absenkuy.IzinActivity;
import com.nikitasamantha_10191065.aku_absenkuy.MainActivity;
import com.nikitasamantha_10191065.aku_absenkuy.R;

public class BerandaFragment extends Fragment {

    LinearLayout btnIzin, btnAktivitas, btnDataAbsen;

    //Konstukter gak usah diapus
    public BerandaFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_beranda, container, false);
        btnIzin = view.findViewById(R.id.btnIzin);
        btnIzin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), IzinActivity.class);
                startActivity(intent);
            }
        });
        btnAktivitas = view.findViewById(R.id.btnAktivitas);
        btnAktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AktivitaspegawaiActivity.class);
                startActivity(intent);
            }
        });
        btnDataAbsen = view.findViewById(R.id.btnDataAbsen);
        btnDataAbsen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), DataabsenActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}